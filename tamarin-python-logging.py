"""
Tamarin Execution/Logging tool for the HPC Clusters at the University of Luxembourg
    Maintained by Zach Smith (ZSmith on Gitlab)

This script bypasses some of the issues associated with executing Tamarin on
University machines. In particular:

    - Tamarin oracles are traditionally run as python files, but on HPC
        machines it instead seems to treat them as shell scripts. A wrapper
        shell script is created which simply runs the oracle directly.

    - Tamarin's non-termination can be frustrating in scenarios where the
        script is being run remotely, since you won't know what causes it to
        get stuck in its thought process. This outputs a "rotating" log 
        containing the most recent thoughts of Tamarin which can be examined
        after completion (or failure).

For more extensive instructions (i.e. how to set up Tamarin on the HPC machines
in the first place), check the git repository at 

"""

import logging
import os
import subprocess
from logging.handlers import RotatingFileHandler
import sys

#----------------------------------------------------------------------
# Returns a handler to a logger at the given path
def create_rotating_log(path):
    """
    Creates a rotating log
    """
    logger = logging.getLogger("Rotating Log")
    logger.setLevel(logging.INFO)
    
    # add a rotating handler
    handler = RotatingFileHandler(path, maxBytes=500000,
                                  backupCount=2)
    logger.addHandler(handler)
    
    return logger
        
#----------------------------------------------------------------------

def run_main():
    if len(sys.argv) < 2:
        print("Please call with a tamarin filename")
        return False

    tamarinFilename = sys.argv[1]
    if not os.path.exists(tamarinFilename):
        print("Tamarin file not found")
        return False

    oracleFilename = False
    if len(sys.argv) == 3:
        oracleFilename = sys.argv[2]
        print("Running with an oracle file")
        if not os.path.exists(oracleFilename):
            print("Oracle file not found")
            return False
    else:
        print("Running without an oracle file")

    # We're banking on the tamarin file ending in .spthy
    results_log_name = tamarinFilename[:-6]+"-results.log"
    thoughts_log_name = tamarinFilename[:-6]+"-thoughts.log"
    oracle_wrapper_name = "oracleWrapper.sh"

    # Flush logs from previous execution
    if os.path.exists(thoughts_log_name):
        os.remove(thoughts_log_name)
    if os.path.exists(results_log_name):
        os.remove(results_log_name)

    processArgs = ["tamarin-prover"
        , tamarinFilename
        , "--prove"
    ]

    # If we stated an oracle file, we need to create
    # a baby batch script to launch the oracle file
    # because the HPC is **weird**
    if oracleFilename:
        with open(oracle_wrapper_name, 'w') as file:
            file.write("python "+oracleFilename+" $1")
        # bash files need at least X permissions so tamarin can call them
        os.chmod(oracle_wrapper_name, 0o755)
        processArgs.append("--heuristic=O")
        processArgs.append("--oraclename="+oracle_wrapper_name)

    processArgs.append("--prove")

    # This file could be very long!
    # We only want the freshest output
    thoughts_log = create_rotating_log(thoughts_log_name)

    # This log will be a reasonable size
    results_log = open(results_log_name, 'w')  

    proc=subprocess.Popen(processArgs,
        stderr=subprocess.PIPE,
        stdout=results_log
    )

    for line in proc.stderr:
        thoughts_log.info(line.strip())
    proc.wait()

    thoughts_log.info("-----------------------------------------")
    thoughts_log.info("Tamarin Execution Successfully Terminated")
    thoughts_log.info("-----------------------------------------")


if __name__ == "__main__":
    run_main()
