# Tamarin Prover on the Luxembourg HPC

The University of Luxembourg offers a HPC cluster for researchers.
For reasons which are beyond my understanding, some parts of the infrastucture of a
supercomputer means that the [Tamarin prover tool](https://tamarin-prover.github.io/)
does not always behave as well as expected.

This repository contains a walkthrough and some scripts that I used to get the HPC
behaving for me.

For the short version:

- Download tamarin and maude
- Set their permissions and add them to `PATH`
- Download the logging script (written in python) attached to this repo
- Use `sbatch` on the HPC to launch a shell script which runs the script,
    along with your settings for how many cores to use etc.

## Installation/Setup

Tamarin can be downloaded as a binary, and requires Maude to run, which can also
be downloaded as a binary. The Tamarin website lists Graphviz as a requirement,
but this is only necessary if you're running the UI, whereas the plan here is to run it in command line mode.

We can download and extract Tamarin and Maude to local folders. Tamarin prefers
a specific version of Maude, which is getting increasingly awkward to find
as the main release is now updated past that version.
We also need to update permissions for the programs (e.g. chmod 755).
The Maude executable has filename is maude.linux64: Tamarin explicitly
looks for the file `maude`, so it is necessary to rename it.

```sh
mkdir ~/temporary-tamarin-setup
cd ~/temporary-tamarin-setup
mkdir ~/bin
wget https://github.com/tamarin-prover/tamarin-prover/releases/download/1.4.1/tamarin-prover-1.4.1-linux64-ubuntu.tar.gz
tar -xzvf tamarin-prover-1.4.1-linux64-ubuntu.tar.gz
wget http://maude.cs.illinois.edu/w/images/5/5d/Maude-2.7.1-linux.zip
unzip Maude-2.7.1-linux.zip

mv maude.linux64 maude
chmod 755 ./*
mv ./*.maude ~/bin/
mv ./maude ~/bin/
mv ./tamarin-prover ~/bin/
```

From here (after swapping into an interactive session), we can test basic functionality:

```
tamarin-prover test
```

For convenience we can add Tamarin and Maude to PATH, so that we don't have
to deal with awkward folder navigation. The easiest way seems to be
to modify \~/.bash_profile, adding at the bottom:


```sh
PATH=$PATH:$HOME/bin
export PATH
```

## Oracle Files

A common technique when using Tamarin is to write an "oracle".
Tamarin uses heuristic techniques for its analysis: an oracle
guides the heuristic and suggests which routes to go down.
Heuristics are usually written as python files: Tamarin
has some line somewhere like:

```sh
exec python path_to_oracle arguments
```

For some reason which I really don't understand, on the HPC machines
the oracle file will be interpreted as a shell file, regardless of
any instructions (e.g. adding `#!usr/bin/python`).
We can this by writing a one-liner bash script:

oracleWrapper.sh
```sh
python ./oracleFile.py $1
```

This fixed everything up nicely.

# Reading Output

Tamarin produces output to stdout and stderr. stdout generally contains
information about the results of the execution - completed proofs,
summaries, etc... We can pipe the output of stdout as part of our call
to tamarin-prover in order to read the results:

```sh
tamarin-prover --prove path-to-file 1>tamarinOutput.txt
```

This is great if we have a file to test that we're confident will terminate.
However, a big danger when running Tamarin is non-termination. This is usually
because the prover gets stuck in some loop during its analysis. For this reason,
Tamarin outputs to stderr what it is "thinking". When running Tamarin on a
local machine, we can read stderr in order to try and figure out what the problem
is. This is a problem on the HPC, though:

1) If we're in an interactive session, we can view the output. However, execution
is significantly slowed down - it appears to be the case that Tamarin will wait
on the ssh connection to make sure that we're receiving the full output, making
analysis time dependent on ping. My tiny 4-core laptop can beat the HPC in this case!

2) If we're using a batch session, we could try piping the output to a log.
However, Tamarin produces a lot of output. Given that it's not uncommon to
have a script that will run for hours, the log file would end up being unacceptably large.

To solve this I wrote a Python script in order to invoke Tamarin as a
subprocess and feed the output into a rotating log. It is on this repo as
`tamarin-python-logging.py`

With this, I can have my "main" batch script
(that I will run on the HPC) invoke this python script.
The end result looks something like this:

```sh
#!/bin/sh
#SBATCH --mail-type=end,fail
#SBATCH --mail-user=<my email address>
#SBATCH -c 24
#SBATCH --mem=64GB
#SBATCH --time=11:59:0
#SBATCH -p batch
#SBATCH --qos=qos-batch

python ../tamarin-python-logging.py\
        <path-to-tamarin-file>\
        <path-to-oracle>
```